# SimExCaP #
## Simulação de Experimento e Cálculo de Probabilidades ##

Este arquivo descreve os passos necessários para executar a aplicação SimExCaP.

### Apresentação ###

A tarefa consiste em simular um experimento repetidas vezes e calcular a probabilidade de ocorrência de cada um dos valores de dados aleatórios.

Para isso precisamos da quantidade de dados que serão usados no experimento, do número de vezes que o experimento será realizado e do número de faces de cada dado a ser utilizado.

Essas informações deverão ser enviadas ao programa através de um arquivo que deve seguir o seguinte padrão:

        <quantidade de dados> <número de repetições do experimento>
        <numero de faces do dado 1> <numero de faces do dado 2> ... <numero de faces do dado n>

Veja um exemplo de arquivo válido:

        6 10
        6 6 8 8 10 20

É possível encontrar arquivos de entrada válidos na pasta **tests**.

O programa foi implementado em linguagem Java com o auxílio da IDE Eclipse. Os códigos estão bem documentados e podem ser facilmente entendidos. A classe principal para entender o experimento é a *Simulador*.

### Como executar? ###

1. [Baixe aqui](https://bitbucket.org/rubemkalebe/simexcap/downloads) este repositório.
2. Extraia o arquivo .zip que foi baixado anteriormente.
3. Navegue, pelo terminal (do Windows ou do Linux), até a pasta gerada anteriormente.
4. Execute o programa com o seguinte comando "java -jar SimExCaP.jar tests/in1.in". Observe que "tests/in1.in" pode ser trocado por qualquer caminho para um arquivo válido. Esse é apenas um arquivo de exemplo que obedece o padrão do programa.
5. Analise os resultados no arquivo *Results.out* que foi gerado após a execução do programa (a cada nova execução do programa esse arquivo é sobrescrito).

IMPORTANTE: Este programa só foi testado em ambiente Linux.

### Equipe de desenvolvimento ###

* Rubem Kalebe (rubemkalebe@gmail.com)