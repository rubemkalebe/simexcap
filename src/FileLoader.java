import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Classe responsável pela leitura do arquivo de entrada.
 * 
 * @author Rubem Kalebe
 * @version 16.04.2015
 */

public class FileLoader {

	// Caminho do arquivo de entrada.
	private String path;
	
	/**
	 * Construtor da classe.
	 * @param path Caminho do arquivo de entrada
	 */
	public FileLoader(String path) {
		this.path = path;
	}
	
	/**
	 * Lê as informações sobre o cenário da simulação.
	 * @param c Cenário
	 * @throws java.util.NoSuchElementException Caso o arquivo esteja incompleto
	 */
	public void read(Cenario c) {
		try {
			Scanner scan = new Scanner(new FileReader(path));
			if(readNumeroDados(c, scan) && readExperimentos(c, scan) 
					&& readFaces(c, scan)) {
				scan.close();
			} else {
				System.err.println("Problema na leitura do arquivo \'" 
						+ path + "\'");
				scan.close();
				throw new NoSuchElementException("Estão faltando informações" +
						"sobre o experimento");
			}
		} catch(FileNotFoundException e) {
			System.err.println("Erro ao abrir arquivo \'" + path + "\'");
		}
	}

	private boolean readNumeroDados(Cenario c, Scanner scan) {
		if(scan.hasNextInt()) {
			c.setNumeroDados(scan.nextInt());
			return true;
		} else {
			return false;
		}		
	}
	
	private boolean readExperimentos(Cenario c, Scanner scan) {
		if(scan.hasNextInt()) {
			c.setExperimentos(scan.nextInt());
			return true;
		} else {
			return false;
		}
	}
	
	private boolean readFaces(Cenario c, Scanner scan) {
		int[] v = new int[c.getNumeroDados()];
		for(int i = 0; i < c.getNumeroDados(); i++) {
			if(scan.hasNext()) {
				v[i] = scan.nextInt();
			} else {
				return false;
			}			
		}
		c.setFaces(v);
		return true;
	}
	
	/**
	 * 
	 * @return Caminho do arquivo de entrada
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Altera o caminho do arquivo de entrada.
	 * @param path Novo caminho do arquivo de entrada
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
