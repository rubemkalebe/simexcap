/**
 * Classe que modela um cenário de simulação, contendo os principais elementos.
 * 
 * @author Rubem Kalebe
 * @version 16.04.2015
 */

public class Cenario {

	// Número de dados
	private int numeroDados;
	
	// Vetor contendo o número de faces de cada dado presente
	private int[] faces;
	
	// Numero de vezes que o experimento será feito.
	private int experimentos;
	
	/**
	 * Construtor da classe.
	 */
	public Cenario() {
		numeroDados = 0;
		faces = null;
		experimentos = 0;
	}

	public int getNumeroDados() {
		return numeroDados;
	}

	public void setNumeroDados(int numeroDados) {
		this.numeroDados = numeroDados;
	}

	public int[] getFaces() {
		return faces;
	}

	public void setFaces(int[] faces) {
		this.faces = faces;
	}

	public int getExperimentos() {
		return experimentos;
	}

	public void setExperimentos(int experimentos) {
		this.experimentos = experimentos;
	}

}
