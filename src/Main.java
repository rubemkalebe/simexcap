/**
 * Classe principal.
 * 
 * @author Rubem Kalebe
 * @version 16.04.2015
 */

public class Main {

	public static void main(String[] args) {

		if(args.length > 0) {
			Cenario c = new Cenario();
			FileLoader loader = new FileLoader(args[0]);
			loader.read(c);
			Simulador simulador = new Simulador(c);
			simulador.simula();
		} else {
			System.err.println("Essa versão só funciona a partir da leitura de um arquivo!" +
					" Caso não tenha um arquivo, você pode enviar o arquivo de teste padrão" +
					" localizado em tests/in1.in");
		}		

	}

}
