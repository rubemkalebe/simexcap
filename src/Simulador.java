import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Classe responsável pela simulação.
 * 
 * @author Rubem Kalebe
 * @version 16.04.2015
 */

public class Simulador {

	// Cenário da simulação
	private Cenario c;
	
	// Estrutura para armazenar ocorrência de cada face
	int[] results;
	
	/**
	 * Construtor.
	 * @param c Cenario da simulação
	 */
	public Simulador(Cenario c) {
		this.c = c;
		results = new int[maiorFace(c.getFaces()) + 1];
		for(int i = 1; i < results.length; i++) results[i] = 0;
	}
	
	/**
	 * Executa a simulação.
	 */
	public void simula() {
		final String filename = "Results.out";
		try {			
            int v[] = c.getFaces();
			FileWriter writer = new FileWriter(filename);
            writer.write("Experimentos:\n");
            for(int i = 0; i < c.getExperimentos(); i++) {
            	int dado = geraNumeroAleatorio(c.getNumeroDados());
            	int valor = geraNumeroAleatorio(v[dado]);
            	valor += 1;
            	writer.write(Integer.toString(v[dado]) + " | " + Integer.toString(valor) + "\n");
            	results[valor] += 1;
            }
            writer.write("\nProbabilidades:\n");
            for(int i = 1; i < results.length; i++) {
            	double px = (double) results[i] / c.getExperimentos();
            	writer.write("P(" + Integer.toString(i) + "): " +
            			Double.toString(px * 100) + "\n");
            }
            writer.close();
        }
        catch(IOException e) {
            System.err.println("Erro ao salvar arquivo \' " + filename + "\'");
        }
	}
	
	private int geraNumeroAleatorio(int x) {
		Random gerador = new Random();
		return gerador.nextInt(x); // retorna um inteiro entre 0 e x-1		
	}

	private int maiorFace(int[] v) {
		int maior = v[0];
		for(int i = 1; i < v.length; i++) {
			if(v[i] > maior) {
				maior = v[i];
			}
		}
		return maior;
	}
	
	public Cenario getC() {
		return c;
	}

	public void setC(Cenario c) {
		this.c = c;
	}

}
